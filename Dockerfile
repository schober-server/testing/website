FROM docker.io/nginx:1.27.4@sha256:9d6b58feebd2dbd3c56ab5853333d627cc6e281011cfd6050fa4bcf2072c9496

COPY nginx-config/ /etc/nginx
COPY main/ /usr/share/nginx/html

ENTRYPOINT ["nginx", "-g", "daemon off;"]

